\documentclass[aspectratio=169,9pt]{beamer}

% Citations (ornery)
\usepackage[backend=bibtex,maxnames=100,doi=false]{biblatex} %from macports texlive-bibtex-extra
\addbibresource{refs.bib}
\renewbibmacro{in:}{} % remove "In:" for journal
\renewcommand{\footnotesize}{\tiny}

\newcommand{\footcomma}{\textsuperscript{,}}

% PETSc colors
\definecolor{petsc_dark_grey}{HTML}{404243}
\definecolor{petsc_green}{HTML}{bacc33}
\definecolor{petsc_red}{HTML}{bc3b3a}
\definecolor{petsc_teal}{HTML}{6ca39e}
\definecolor{petsc_orange}{HTML}{d86035}
\definecolor{petsc_blue}{HTML}{305e89}

\definecolor{darkblue}{rgb}{0,0,.6}
\definecolor{darkred}{rgb}{.6,0,0}
\definecolor{darkgreen}{rgb}{0,.6,0}
\definecolor{red}{rgb}{.98,0,0}
\definecolor{lightgrey}{rgb}{0.98,0.98,0.98}


% Listings
\usepackage{listings}
\usepackage{xcolor}
\lstloadlanguages{C++}
\lstset{%
  language=C++,
  basicstyle=\small\ttfamily,
  commentstyle=\itshape\color{darkgreen},
  keywordstyle=\bfseries\color{darkblue},
  stringstyle=\color{darkred},
  showspaces=false,
  showtabs=false,
  columns=fixed,
  backgroundcolor=\color{lightgrey},
  numbers=none,
  frame=single,
  numberstyle=\tiny,
  breaklines=true,
  showstringspaces=false,
  xleftmargin=0.1cm,
  xrightmargin=3em
}%

% Customize hyperref here
% color links, but not internal links (confusing..)
\hypersetup{colorlinks=true,linkcolor=,urlcolor=magenta}

% Beamer Customization
\setbeamertemplate{navigation symbols}{} % Remove the navigation symbols
\setbeamertemplate{sections/subsections in toc}[default] %Turn off ugly toc numbering
\setbeamertemplate{itemize subitem}[triangle]
\setbeamertemplate{itemize item}[triangle]
\setbeamertemplate{enumerate items}[default]
\setbeamertemplate{footline}[page number]{}

\author[Patrick Sanan]{\textbf{Patrick Sanan} (Argonne National Laboratory, ETH Zurich)\\ On behalf of the \href{https://petsc.org/release/community/petsc_team/}{PETSc team} (Particular thanks to Richard Mills)}

\title[PETSc and GPUs]{Using GPUs with PETSc}
\date[2022-02-28]{CIG Developers's Workshop, 2022-02-28}

\begin{document}

\begin{frame}[fragile]
\titlepage
  \begin{center}
    \includegraphics[width=0.4\textwidth]{images/PETSc-TAO_CMYK.pdf}\\
    Slide source and full example configuration and logs at
    \url{https://gitlab.com/psanan/petsc-gpu-cig-2022-02-28}
  \end{center}
\end{frame}

\begin{frame}[fragile]
\frametitle{PETSc - the Portable, Extensible Toolkit for Scientific computation}
  \begin{center}
    \includegraphics[width=0.9\textwidth]{images/petsc_overview.png}
  \end{center}
\end{frame}

\begin{frame}[fragile]
\frametitle{This Presentation}
    \begin{itemize}
        \item Quickly motivate the use of GPUs for scientific computing
        \item Show you how a first example with GPUs in PETSc
        \item That's it, since time is short, but tell you how to get more help and information.
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{What's a GPU?}
    \begin{itemize}
        \item GPU = ``Graphics Processing Unit'', designed to:
            \begin{itemize}
                \item Do lots of small, simple computations on pixels or triangles/quads
                \item Do so fast enough to draw things on a monitor
                \item Do so without consuming too much power
                \item Ultimately produce 2D arrays of values in buffers to be displayed
            \end{itemize}
        \item Thus, they
            \begin{itemize}
                \item Prioritize \emph{throughput} over \emph{latency}
                \item Perform best when many cores are active (the workload is highly parallel)
                \item Have simpler (more energy-efficient) processing elements than CPUs
                \item Have large numbers of processing elements
                \item Don't need fast data transfers anywhere except to and from their own memory.
            \end{itemize}
        \item Scientific computing uses GPUs as General Purpose GPUs (GPGPUs)
        \item GPUs are now available which support things, like double precision floating point numbers and fast GPU-GPU communication, that the original GPUs had little need for.
    \end{itemize}
\end{frame}

\begin{frame}[fragile]

\frametitle{Why does PETSc care about GPUs?}
\begin{itemize}
    \item GPUs can do more computational work per dollar or Watt than CPUs, for certain workloads
    \item Some of these workloads align with the kinds of solvers PETSc focuses on
    \item They are increasingly available/usable
    \item So, supporting them can help our users solve their problems faster.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Why it's not so easy, under the hood}
    \begin{itemize}
        \item Performance (these days) is so much about data movement, and GPUs can have severe data movement bottlenecks, so the library needs to have a tightly-integrated abstraction of GPUs and cannot completely hide their existence from the user.
        \item This is the same situation with two of the other main things that PETSc helps manage
            \begin{enumerate}
                \item MPI parallelism - not all solvers can be parallelized this way.
                \item Domain Management (\lstinline{DM}) - scalable solvers often need to know the geometry of the domain
            \end{enumerate}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{How PETSc uses GPUs}
  \begin{itemize}
      \item Provides several new implementations of PETSc's \lstinline{Vec} distributed vector class \footfullcite{MillsEtAl2020} which allow data storage and manipulation in device (GPU) memory
    \item A ``lazy-mirror'' model
    \item Embue all \lstinline{Vec} (and \lstinline{Mat}) objects with the ability to track the state of a second "offloaded" copy of the data, and synchronize these two copies of the data (only) when required.
  \end{itemize}
  \begin{block}{Host and Device Data}
  \begin{lstlisting}
struct _p_Vec {
  ...
  void             *data;        // host buffer
  void             *spptr;       // device buffer
  PetscOffloadMask offloadmask;  // which copies are valid
};
  \end{lstlisting}
  \end{block}

  \begin{block}{Possible Flag States}
  \begin{lstlisting}
  typedef enum {PETSC_OFFLOAD_UNALLOCATED,
                PETSC_OFFLOAD_GPU,
                PETSC_OFFLOAD_CPU,
                PETSC_OFFLOAD_BOTH} PetscOffloadMask;
  \end{lstlisting}
  \end{block}
\end{frame}


\begin{frame}[fragile]
\frametitle{Configuring PETSc for use with GPUs}
  \begin{itemize}
      \item  Key: you need GPU-aware MPI (e.g. CUDA-aware MPI) if you want good performance on more than one GPU. Your cluster hopefully has this, and locally you can use \lstinline{--download-openmpi}
      \item Key \lstinline{./configure} options
            \begin{itemize}
              \item \lstinline{--with-cuda}
              \item \lstinline{--download-kokkos --download-kokkos-kernels}
            \end{itemize}
          \item Figure out the basics of how one compiles and runs code in C and CUDA (or HIP or SYCL..) on your machine of interest and use this to inform your configuration.
        \item But do not suffer too much! By clearly documenting your attempt in an email to \texttt{petsc-maint@mcs.anl.gov}, with \lstinline{configure.log} and \lstinline{make.log}, most problems can be quickly resolved.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Single-GPU system and configuration used here}
    \begin{itemize}
        \item Red Hat Linux \lstinline{4.18.0-240.22.1.el8_3.x86_64}
        \item PETSc \lstinline{main} \lstinline{v3.16.4-987-gac50153c78} % git describe
        \item GPU: NVIDIA Titan Xp (expensive, advertised peak memory bandwidth: 548 GB/s)
        \item CPU: Intel® Core™ i5-4460 (cheap, advertised peak memory bandwidth: 25.6 GB/s)
        \item PETSc configuration (not all required for these demos)
            \begin{lstlisting}[language=bash]
./configure \
--with-cc=gcc --with-cxx=g++ --with-fortran=gfortran \
--with-fortran-bindings=0 --download-fblaslapack=1 \
--download-openmpi \
--download-kokkos --download-kokkos-kernels \
--download-viennacl --download-libceed --download-strumpack \
--download-scalapack --download-metis --download-suitesparse \
--download-cmake \
--with-cuda=1 \
--with-debugging=0 \
--COPTFLAGS=\"-g -O3 -march=native\" \
--CXXOPTFLAGS=\"-g -O3 -march=native\" \
--FOPTFLAGS=\"-g -O3 -march=native\" \
--CUDAOPTFLAGS="-O3" \
            \end{lstlisting}
    \end{itemize}
\end{frame}



\begin{frame}[fragile]
\frametitle{Example}

\begin{block}{SNES ex19}
  Velocity-vorticity formulation for nonlinear driven cavity. This is what runs with \lstinline{make check} after you configure PETSc, and can be solved nicely leveraging multigrid.
\vspace{2em}
\begin{center}
\begin{minipage}{.25 \textwidth}
\includegraphics[width=\textwidth]{images/DrivenCavitySolution}
\end{minipage}
\begin{minipage}{.58 \textwidth}
\begin{align*}
        - \Delta U - \partial_y \Omega &= 0 \\
        - \Delta V + \partial_x \Omega &= 0 \\
        - \Delta \Omega + \nabla \cdot ([U \Omega, V \Omega]) - \mathrm{Gr}\ \partial_x T &= 0 \\
        - \Delta T + \mathrm{Pr}\ \nabla \cdot ([U T, V T]) &= 0
\end{align*}
  \begin{itemize}
  \item No-slip (zero Dirichlet) boundary conditions for $U$,  $V$,
  \item Vorticity $\Omega$ on the boundary from $\Omega = - (\nabla_y U + \nabla_x V)$,
  \item insulated top and bottom, and
  \item fixed temperature on the left and right.
  \end{itemize}
\end{minipage}
\end{center}
\end{block}
\end{frame}

\begin{frame}[fragile]
\frametitle{Example}
\begin{lstlisting}[language=bash]
$ # Set PETSC_DIR and PETSC_ARCH (if not using a prefix install)
$ cd $PETSC_DIR/src/snes/tutorials
$ make ex19
$ $PETSC_DIR/lib/petsc/bin/mpiexec -n 1 ./ex19 \
  -da_refine 7 \
  -snes_monitor -snes_converged_reason \
  -pc_type mg -mg_levels_pc_type jacobi
\end{lstlisting}
\begin{lstlisting}[language=]
lid velocity = 0.000106281, prandtl # = 1., grashof # = 1.
  0 SNES Function norm 1.036007954337e-02
  1 SNES Function norm 3.547498241914e-05
  2 SNES Function norm 1.031529071036e-09
  3 SNES Function norm 1.145567739037e-14
Nonlinear solve converged due to CONVERGED_FNORM_RELATIVE iterations 3
Number of SNES iterations = 3
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
\frametitle{Example: Log}
Run a CPU-only case \footnote{Using $2$ ranks since PETSc's \lstinline{make streams} implies that this is
enough to saturate the available memory bandwidth.} and add PETSc's \lstinline{-log_view} - more in the \href{https://petsc.org/release/docs/manual/profiling/}{Profiling chapter in the manual}.
\begin{lstlisting}[language=bash]
$ # Set PETSC_DIR (and PETSC_ARCH if not using a prefix install)
$ cd $PETSC_DIR/src/snes/tutorials
$ make ex19
$ $PETSC_DIR/lib/petsc/bin/mpiexec -n 2 ./ex19 \
  -da_refine 7 \
  -log_view :log_7_ref.txt \
  -pc_type mg -mg_levels_pc_type jacobi
\end{lstlisting}
If we look in \lstinline{log_7_ref.txt}, we can see the time for the main nonlinear solve:
\begin{lstlisting}[language=]
--------------------------------------------- ...
Event                Count      Time (sec)    ...
                   Max Ratio  Max     Ratio   ...
--------------------------------------------- ...
...
SNESSolve              1 1.0 4.2338e+00 1.0 ...
...
\end{lstlisting}

\end{frame}

\begin{frame}[fragile]
\frametitle{Using command-line options to run using CUDA}
Once PETSc is configured \lstinline{--with-cuda}, by simply using command line options,
one may move some operations to the GPU. This is not expected to give optimal performance,
but on some systems (like the one I'm using here), this does give some ``free speedup'':
\begin{lstlisting}[language=bash]
$ # Set PETSC_DIR and PETSC_ARCH (if not using a prefix install)
$ cd $PETSC_DIR/src/snes/tutorials
$ make ex19
$ $PETSC_DIR/lib/petsc/bin/mpiexec -n 1 ./ex19 \
  -da_refine 7 \
  -log_view :log_7_cuda.txt \
  -pc_type mg -mg_levels_pc_type jacobi \
  -dm_vec_type cuda -dm_mat_type aijcusparse
\end{lstlisting}
\begin{lstlisting}[language=]
--------------------------------------------- ...
Event                Count      Time (sec)    ...
                   Max Ratio  Max     Ratio   ...
--------------------------------------------- ...
...
SNESSolve              1 1.0 1.4922e+00 1.0 ...
...
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
\frametitle{Using command-line options to run using CUDA via Kokkos}
Once PETSc is configured \lstinline{--with-cuda --download-kokkos --download-kokkos-kernels}, one can use
    \lstinline{Vec}s and \lstinline{Mat}s backed by Kokkos views. The big advantage
    here is that you can implement operations on this data which are backend-portable,
    and can also interface with optimized libraries like LibCEED\footnote{\url{https://ceed.exascaleproject.org/libceed/}} which provide Kokkos-based
    kernels (amongst others).
\begin{lstlisting}[language=bash]
$ # Set PETSC_DIR and PETSC_ARCH (if not using a prefix install)
$ cd $PETSC_DIR/src/snes/tutorials
$ make ex19
$ $PETSC_DIR/lib/petsc/bin/mpiexec -n 1 ./ex19 \
  -da_refine 7 \
  -log_view :log_7_kokkos.txt \
  -pc_type mg -mg_levels_pc_type jacobi \
  -dm_vec_type kokkos -dm_mat_type aijkokkos
\end{lstlisting}
\begin{lstlisting}[language=]
--------------------------------------------- ...
Event                Count      Time (sec)    ...
                   Max Ratio  Max     Ratio   ...
--------------------------------------------- ...
...
SNESSolve              1 1.0 2.0729e+00 1.0 ...
...
\end{lstlisting}
\end{frame}


\begin{frame}[fragile]
\frametitle{Comparing Performance}
\begin{itemize}
\item We know from earlier that we expect GPUs to work best for large problems, so let's increase
    the problem size a bit by adding more levels of refinement (and levels to our multigrid hierarchy).

\item SNES Solve times:
\begin{center}
\begin{tabular}{ccccc}
Levels & DOF       & Time (ref) & Time (CUDA) & Time (Kokkos) \\
\hline
7      &   592900  & 4.23       & 1.49        & 2.07 \\
8      &  2365444  & 18.3       & 5.32        & 7.06 \\
9      &  9449476  & 76.5       & 21.3        & 28.1 \\
\hline
\end{tabular}
\end{center}

\item Let's take a look at the 9-level logs to see where the speedup is really happening, e.g. \lstinline{MatMult}

\begin{center}
\begin{tabular}{ccccc}
Levels & DOF       & Time (ref) & Time (CUDA) & Time (Kokkos) \\
\hline
9      &  9449476  &  42.0      &    3.76      & 3.80 \\
\hline
\end{tabular}
\end{center}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Seeing CPU-GPU transfers in the logs}
The most challenging part of extracting good performance from a PETSc solve using
GPUs is to minimize CPU-GPU transfers. This can be a bit tricky with the lazy-mirror model,
but \lstinline{--log-view} has recently been improved to show transfers in every log stage.

\begin{lstlisting}[language=]
------------------ ... --------------------------------
Event              ...  - CpuToGpu -   - GpuToCpu - GPU
                   ...  Count   Size   Count   Size  %F
------------------ ... --------------------------------

...
SNESSolve          ...  1430 2.19e+04  787 7.94e+03 91
...

\end{lstlisting}

See the \href{https://gitlab.com/psanan/petsc-gpu-cig-2022-02-28/-/blob/main/runs/log_9_cuda.txt}{full log on the Gitlab repository for this talk}.

\end{frame}


\begin{frame}[fragile]
\frametitle{How to get support}
  \begin{itemize}
    \item \href{https://petsc.org}{petsc.org}
    \item Our \href{https://petsc.org/release/community/mailing/}{mailing lists} are very responsive. Send your \lstinline{-log_view} output if you can.
      \begin{itemize}
        \item \href{mailto:petsc-users@mcs.anl.gov}{petsc-users@mcs.anl.gov} (public, preferred)
        \item \href{mailto:petsc-maint@mcs.anl.gov}{petsc-maint@mcs.anl.gov} (private, use if you have big or sensitive attachments)
      \end{itemize}
  \end{itemize}
  Thank you!
\end{frame}


\end{document}
