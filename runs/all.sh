#!/usr/bin/env sh

# Run all the experiments

# Set PETSC_DIR first to a build configured with CUDA and Kokkos
if [ -z "$PETSC_DIR" ]; then
  printf "PETSC_DIR must be defined in your enviroment\n"
  exit 1
fi

# Example
source_dir="$PETSC_DIR/src/snes/tutorials"
ex_name=ex19

# Build example and return here
cd "$source_dir" && make $ex_name && cd -

# Remove all logs
rm -f log*.txt

for n_levels in $(seq 7 9); do
  # CPU
  $PETSC_DIR/lib/petsc/bin/petscmpiexec -n 2 \
  "$source_dir/$ex_name" \
  -pc_type mg -mg_levels_pc_type jacobi \
  -da_refine ${n_levels} \
  -log_view :log_${n_levels}_ref.txt \

  # CUDA
  $PETSC_DIR/lib/petsc/bin/petscmpiexec -n 1 \
  "$source_dir/$ex_name" \
  -pc_type mg -mg_levels_pc_type jacobi \
  -da_refine ${n_levels} \
  -dm_vec_type cuda -dm_mat_type aijcusparse \
  -log_view :log_${n_levels}_cuda.txt \

  # Kokkos
  $PETSC_DIR/lib/petsc/bin/petscmpiexec -n 1 \
  "$source_dir/$ex_name" \
  -pc_type mg -mg_levels_pc_type jacobi \
  -da_refine ${n_levels} \
  -dm_vec_type kokkos -dm_mat_type aijkokkos \
  -log_view :log_${n_levels}_kokkos.txt \

done
